package com.example.motionlayouy5

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.Indication
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Share
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ExperimentalMotionApi
import androidx.constraintlayout.compose.MotionLayout
import androidx.constraintlayout.compose.MotionScene
import com.example.motionlayouy5.ui.theme.MotionLayouy5Theme

class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterialApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Surface(color = Color.White) {
                CollapsableToolbar()
            }
        }
    }
}

@Composable
fun ScrollableContent() {
    LazyColumn(
        modifier = Modifier.padding(bottom = 8.dp)
    ) {
        items(25) {
            WalletItem(
                name ="bitcoin${it}",
                image = painterResource(id = R.drawable.btc),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 16.dp, top = 8.dp, bottom = 8.dp)
                    .clickable(
                        onClick = { },
                    )

            )
        }
    }
}

@OptIn(ExperimentalMotionApi::class)
@Composable
fun MotionLayoutView(progress: Float, scrollableBody: @Composable () -> Unit) {

    val context = LocalContext.current
    val motionScene = remember {
        context.resources.openRawResource(R.raw.motion_scene)
            .readBytes()
            .decodeToString()
    }

    MotionLayout(
        motionScene = MotionScene(content = motionScene),
        progress = progress,
        modifier = Modifier.fillMaxWidth()
    ) {

        val properties = motionProperties("title")

        Image(
            painter = painterResource(id = R.drawable.myself),
            contentDescription = "",
            modifier = Modifier
                .layoutId("my_image")
                .clip(CircleShape)
        )
        Icon(
            imageVector = Icons.Filled.ArrowBack,
            contentDescription = "",
            modifier = Modifier.layoutId("arrow_back")
        )
        Text(
            text = "HHosseini",
            fontWeight = FontWeight.Bold,
            color = properties.value.color("text_color"),
            fontSize = 16.sp,
            modifier = Modifier.layoutId("title")
        )
        Icon(
            imageVector = Icons.Filled.Share,
            contentDescription = "",
            tint = properties.value.color("text_color"),
            modifier = Modifier.layoutId("share")
        )

        Box(modifier = Modifier.layoutId("content")){
            scrollableBody()
        }

    }
}

@Composable
fun WalletItem(name: String, image: Painter, modifier: Modifier) {
    Card(
        modifier = modifier,
        elevation = 8.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Row(modifier = Modifier.fillMaxWidth()) {

            Column(
                modifier = Modifier
                    .weight(0.15f)
                    .padding(8.dp)
                    .wrapContentHeight(),
                horizontalAlignment = Alignment.CenterHorizontally
            )
            {
                CircleImageView(image, size = 32.dp)
            }

            Column(
                modifier = Modifier
                    .weight(0.475f)
                    .padding(8.dp)
            ) {
                androidx.compose.material.Text(
                    modifier = Modifier.padding(bottom = 8.dp),
                    text = name,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold
                )
                androidx.compose.material.Text(
                    modifier = Modifier.padding(bottom = 8.dp),
                    text = "BTC / USD",
                    color = Color.Gray
                )
            }

            Column(
                modifier = Modifier
                    .weight(0.475f)
                    .padding(top = 8.dp, bottom = 8.dp, end = 16.dp),
                horizontalAlignment = Alignment.End
            ) {
                androidx.compose.material.Text(
                    modifier = Modifier.padding(bottom = 8.dp),
                    text = "BTC 0.01254",
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold
                )
                androidx.compose.material.Text(
                    modifier = Modifier.padding(bottom = 8.dp),
                    text = "$895",
                    color = Color(0xFF299C0D)
                )
            }
        }
    }
}

@Composable
fun CircleImageView(painter: Painter, size: Dp) {
    Image(
        painter = painter,
        contentDescription = "",
        modifier = Modifier
            .size(size)
            .clip(CircleShape)
//            .border(
//                width = 3.dp,
//                shape = CircleShape,
//                color = Color.Blue
//            )
    )
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    MotionLayouy5Theme {
        Greeting("Android")
    }
}